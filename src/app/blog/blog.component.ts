import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

  constructor() { }

  list=[
    {icon:true,iconClass:"autorenew",assigned:"fsfsfsdf",designation:"web developer", name: "fsdfsdfsd", pririty:"high",budget:"$5.6k", color:"bg-FF5C6C", },
    {icon:true,iconClass:"face",assigned:"fsfsfsdf",designation:"qa", name: "fsdfsdfsd", pririty:"low",budget:"$5.6k", color:"bg-20AEE3"},
    {icon:true,iconClass:"contact_page",assigned:"fsfsfsdf",designation:"qa", name: "fsdfsdfsd", pririty:"medium",budget:"$5.6k", color:"bg-FF5C6C"},
    {icon:false,iconClass:"",assigned:"fsfsfsdf",designation:"HR", name: "fsdfsdfsd", pririty:"low",budget:"$5.6k", color:"bg-20AEE3"},
    {icon:true,iconClass:"contact_support",assigned:"fsfsfsdf",designation:"web developer", name: "fsdfsdfsd", pririty:"medium",budget:"$5.6k", color:"bg-24D2B5"},
    {icon:false,iconClass:"",assigned:"fsfsfsdf",designation:"web developer", name: "fsdfsdfsd", pririty:"medium",budget:"$5.6k", color:"bg-24D2B5"}
  ]
  ngOnInit(): void {
  }

}
